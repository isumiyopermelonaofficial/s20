// [SECTION] Introduction to JSON  (JavaScript Object Notation)

// JSON -> is a 'Data Representation Format' similar to XML and YAML

// Uses of a JSON data format:

	// => Commonly used for API and Configuation.
	// => JSON is also used in serializing different data types into 'bytes'.

	// What is Serializing
		// => is the process of converting data into a series of 'byte' for easier transmission/transfer of information

		// a 'byte' => is a unit of data that is eight binary digits (1 and 0) that is used to represent a characteds(letters, number, typographics symbols).

		// Benefits => once a piece of data/information has been serialize they become 'lightweight', it becomes a lot easier to transfer or transmit over a network or connection.

// [SECTION] Structure of JSON format

	// => JSON data is similar to the structure of a JS Object.
	// => JS Objects are NOT to be confursed with JSON.
	// SYNTAX:
		/*
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}	
		*/

		// example:
		// {
		// 	"city": "Quezon City",
		// 	"province": "Metro Manila",
		// 	"countyr": "Philippines"
		// }

// [SECTION] JSON Types
	// JSON will accept the following values:
	// 1. Strings
	// 2. Number
	// 3. Boolean
	// 4. null
	// 5. Array
	// 6. Objects

// KEEP THIS IN MIND:
	// anything that you write in JSON, is VALID JAVASCRIPT. 

let employees = [
	{
		"name": "Thonie Fernandex",
		"department": "Instructor",
		"yearEmployed": "2020",
		"ratings": 5.0
	},

	{
		"name": "Carles Quimpo",
		"department": "Instructor",
		"yearEmployed": "2010",
		"ratings": 5.0
	},

	{
		"name": "Isumiyo Permelona",
		"department": "Instructor",
		"yearEmployed": "2020",
		"ratings": 5.0
	},

	{
		"name": "Alvin Estiva",
		"department": "Instructor",
		"yearEmployed": "2021",
		"ratings": 4.0
	}
];

// let users = {
// 	"name": "Kyle",
// 	"favoriteNumber": 7,
// 	"isProgrammer": true,
// 	"hobbies": [
// 		"Weight Lifting",
// 		"Reading Comics",
// 		"Playing the Guitar"
// 	],
// 	"friends": [
// 		{
// 			"name": "Robin",
// 			"isProgrammer":true
// 		}

// 		{
// 			"name": "Daniel",
// 			"isProgrammer":false
// 		}
// 		{
// 			"name": "Oswald",
// 			"isProgrammer":true
// 		}
// 	]
// };

console.table(employees);
//console.log(users);

let application = {
	"name": "javascript server",
	"version": "1.0",
	"description": "server side application done using javascript and node js,",
	"main": "index.js",
	"scripts": {
		"start": "node index.js"
	},
	"keywords":[
		"server",
		"node",
		"backend"
	],
	"author": "John Smith",
	"license": "ISC"
}

console.log(application);
console.log(typeof application);

// [SECTION] Different JSON Methods

	// The JSON objects contains method for parsing and converting data into 'stringified' JSON.

// 1. How to convert a JSON data to the 'STRINGIFIED' version
	// JSON.stringify() -> will allow us to convert a JSON object into all strings.

let jsonString = JSON.stringify(employees);

	// WHAT IS THE USE CASE: This is commonly used when sending HTTP request, where information is required to be sent and received in a "stringified" JSON format.

// 2. How to convert JSON strings into JSON Objects

	// JSON.parse() -> will allow us to convert the stringified object into a JS object.

let jsonObject = JSON.parse(jsonString);
// Upon converting a stringed JSON to JS Obejct, you will now be able to use and access the data stored.
console.log(jsonObject[0].name);
